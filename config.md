#Config 配置
Config 文件夹目前分为以下部分

* mkiso.conf
* apt
* includes.binary
* packages.chroot
* hooks.chroot
* packages.list

##mkiso.conf

mkiso.conf 为ini文件

####Common

* Architecture指定编译环境架构: amd64/i386
* Distribution指定编译环境版本:unstable/stable/testing,也就是deboostrap使用dist
* Use_tmpfs 指定是否使用内存生成iso
* Tmpfs_size 指定内存目标大小

#### Env

* LB_APT 指定使用包管理前端，目前支持apt-get/aptitiude
* APT_OPTIONS 指定软件包管理默认参数
* LB_INITRAMFS 指定livecd启动配置方式，支持live-boot/casper
* LB_DEBCONF_FRONTEND 为dpkg配置前端
* LIVE_IMAGE_NAME 生成iso名称前缀

####Debootstrap

* Mirror 使用仓库地址
* Area deboostrap生成使用section
* Args debootsrap默认参数

####Image

* STATUS 指定为测试版或者正式版
* ISO_INFO iso文件元数据

##apt/

主要针对生成环境apt配置

* apt/sources.list 生成iso sources.list配置，即为{chroot}/etc/apt/sources.list
* apt/preferences
* apt/apt.conf
* apt/*.list 除sources.list外，都将作为生成iso /etc/apt/sources.list.d 列表文件

##packages.list/

指定iso包含软件包列表，支持 packagename:i386类似方式

##packages.chroot/

指定iso包含软件包文件，比如添加仓库未收录的或者自己修改的软件包二进制

##hooks.chroot/

生成iso过程中执行的一下自定义脚本

##inlcudes.binary/

iso根目录下包含文件，包括iso启动界面/iso启动参数等调整

其中 live.cfg.in 和 splash.svg.in 为模板文件
